FROM python:3.12.3-alpine

# Create user with home directory and no password and change workdir
RUN adduser -Dh /api wikiapi
WORKDIR /api
# API will run on port 80

EXPOSE 8085
# Environment variable for default config file location
ENV JASSAPI_CONFIG=config_prod.yaml
ENV PYTHONUNBUFFERED=1

# Install dependencies for install
# RUN apk add --no-cache --virtual .deps \
#         musl-dev python-dev gcc

# Copy files to /api directory, install requirements
COPY ./ /api
RUN pip install -r /api/requirements.txt

# Install jassapi to enable CLI commands
# The -e flag installs links only instead of moving files to /usr/lib
RUN pip install -e /api
RUN mkdir data

# Switch user
# USER wikiapi

# Start jassapi as default
CMD ["wikiapi", "run", "prod"]
