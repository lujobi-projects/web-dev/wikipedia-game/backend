#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# license: AGPL, see LICENSE for details. In addition we strongly encourage
#          you to buy us beer if we meet and you like the software.

"""Install jassapi. Provide the jassapi command."""

from setuptools import setup, find_packages
import re

# The single source of version is in the project settings.
# More: https://packaging.python.org/guides/single-sourcing-package-version/
# Parsing inspired by: https://github.com/pallets/flask/blob/master/setup.py

setup(
    name="wikiapi",
    version="0.1",
    url="",
    author="Ian Boschung",
    author_email="ian@ianbo.ch",
    description=(""),
    license="LICENSE",
    platforms=["any"],
    packages=find_packages(),
    entry_points='''
        [console_scripts]
        wikiapi=wikiapi.cli:cli
    '''
)
