from tornado.web import Application, RequestHandler

import yaml

from os import getenv
from os import mkdir

from wikiapi.SocketHandler import SocketHandler


class ServeClient(RequestHandler):
    def get(self):
        self.render("client.html")


def make_app(debug):

    config_path = getenv('WIKIAPI_CONFIG', 'config.yaml')
    print(config_path)

    with open(config_path, 'r') as stream:
        try:
            settings = yaml.safe_load(stream)
            # print(settings)
        except yaml.YAMLError as exc:
            print(exc)

    urls = [(r"/client", ServeClient),
            (r"/", SocketHandler)]

    return Application(urls, **settings, debug=debug)
