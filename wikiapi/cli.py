from tornado.ioloop import IOLoop

from click import argument, group, Choice

from wikiapi.bootstrap import make_app


@group()
def cli():
    """Manage wikiapi."""


@cli.command()
@argument('mode', type=Choice(['prod', 'dev']))
def run(mode):
    """Run production/development server.
    Two modes of operation are available:
    - dev: Run a development server
    - prod: Run a production server (requires the `bjoern` module)
    """
    if mode == 'dev':
        app = make_app(True)
        app.listen(8086)
        IOLoop.instance().start()

    if mode == 'prod':
        app = make_app(False)
        app.listen(8085)
        IOLoop.instance().start()
