from tornado.websocket import WebSocketHandler

import json
import random
from copy import deepcopy
"""

No State:
    actions: [
        "CREATE_GAME": 
            - arguments: name
            - return value: new game pin
            - next state: WAITING
    ]

WAITING:
    actions: [
        "JOIN_GAME":
            - arguments: name, pin
            - return value: broadcast with all players
            - next state: WAITING
        "START_GAME":
            - arguments: none
            - return value: broadcast with action "game_started"
            - next state: SEARCHING
    ]

SEARCHING:
    actions: [
        "CHOOSE_ARTICLE":
            - arguments: article
            - return value: broadcast to all players with progress indicator, first title to vote if all players have chosen
            - next state: VOTING if all players have chosen, SEARCHING otherwise
    ]

VOTING:
    actions: [
        "VOTE":
            - arguments: name
            - return value: broadcast to all players with progress, next title if all players have voted
            - next state: FINISHED if all articles have been voted, VOTING otherwise
    ]

FINISHED:
    actions: [
        "RESTART":
            - arguments: none
            - return value: broadcast with all players who want to restart, new game pin
            - next state: WAITING in a new game
    ]


"""


def filter_public(game):
    return {'pin': game['pin'], 'players': list(game['players'].keys())}


def filter_score(game):
    return [
        {"name": name, "score":player['score']}
        for name, player in sorted(game['players'].items(),
                                   key=lambda item: item[1]['score'],
                                   reverse=True)
    ]


def evaluate_game(game):
    order = game['order']
    for playername, player in game['players'].items():
        correct = 0
        print(player)
        voting = player['voting']
        print(voting)
        for i in range(len(voting)):
            if order[i] != playername:
                game['players'][voting[i]]['creativity_score'] += 1
                if order[i] == voting[i]:
                    correct += 1
        player['correct_score'] = correct

    for playername, player in game['players'].items():
        player['score'] = player['correct_score'] + player['creativity_score']


# def load_game(pin):
#     fh = open(f"data/{pin}.json", 'r')
#     try:
#         game = json.loads(fh.read())
#         return game
#     except json.JSONDecodeError:
#         print("decode error")

# game = load_game("703599")

# evaluate_game(game)
# print(filter_score(game))


class SocketHandler(WebSocketHandler):
    connections = dict()
    emptyPlayer = {
        'article': '',
        'voting': [],
        'correct_score': 0,
        'creativity_score': 0,
        'score': 0
    }

    # name = None

    def initialize(self):
        self.user = None
        self.pin = None

    def save_game(self, game):
        fh = open(f"data/{game['pin']}.json", 'w')
        fh.write(json.dumps(game))
        fh.close()

    def load_game(self, pin):
        fh = open(f"data/{pin}.json", 'r')
        try:
            game = json.loads(fh.read())
            return game
        except json.JSONDecodeError:
            print("decode error")

    def check_origin(self, origin):
        return True

    def open(self):
        print("onopen")

    def broadcast(self, pin, message, includeSelf=True):
        for conn in self.connections[pin]:
            if (conn != self or includeSelf):
                conn.write_message(message)

    def on_message(self, message):
        try:
            data = json.loads(message)
            print(message)
            if (data['type'] == "CREATE_GAME"):
                if ('name' not in data):
                    self.write_message("Name field missing")
                    return
                # TODO Create Game
                newGame = {
                    'pin': random.randint(10**5, 10**6 - 1),
                    'state': 'OPEN',
                    'active_player': 0,
                    'articles_chosen': [],
                    'players_voted': [],
                    'admin': '',
                    'order': [],
                    'players': {
                        data['name']: deepcopy(self.emptyPlayer)
                    }
                }
                self.connections[newGame['pin']] = [self]
                self.name = data['name']
                self.pin = newGame['pin']
                self.save_game(newGame)
                # message = {'pin': newGame['pin'], 'players': newGame['players']}
                message = {
                    'type': 'CREATE_GAME',
                    'status': 'SUCCESS',
                    'data': filter_public(newGame)
                }
                self.write_message(json.dumps(message))
            elif (data['type'] == "JOIN_GAME"):
                if ('name' not in data):
                    self.write_message("Name field missing")
                    return
                if ('pin' not in data):
                    self.write_message("Game pin field missing")
                    return
                game = self.load_game(data['pin'])
                if game['state'] != 'OPEN':
                    self.write_message(
                        "This game has already started and is not open.")
                    return
                game['players'][data['name']] = deepcopy(self.emptyPlayer)
                self.save_game(game)
                self.connections[game['pin']].append(self)
                self.name = data['name']
                self.pin = game['pin']
                message = {
                    "type": "JOIN_GAME",
                    'status': 'SUCCESS',
                    'data': filter_public(game)
                }
                self.write_message(json.dumps(message))
                message['status'] = 'BROADCAST'
                self.broadcast(game['pin'], json.dumps(message), False)
            elif data['type'] == "START_GAME":
                game = self.load_game(self.pin)
                game['state'] = 'SEARCHING'
                message = {"type": "START_GAME", 'status': 'BROADCAST'}
                self.broadcast(game['pin'], json.dumps(message))
                self.save_game(game)
            elif data['type'] == "CHOOSE_ARTICLE":
                if ('article' not in data):
                    self.write_message("Article field missing")
                    return
                game = self.load_game(self.pin)
                game['players'][self.name]['article'] = data['article']
                game['articles_chosen'].append(self.name)
                message = {}
                if len(game['articles_chosen']) == len(game['players']):
                    for name in game['players']:
                        game['order'].append(name)
                    game['state'] = "VOTING"
                    random.shuffle(game['order'])
                    curPlayer = game['order'][game['active_player']]
                    curTitle = game['players'][curPlayer]['article']
                    message = {
                        "type": "CHOOSE_ARTICLE",
                        'status': 'SUCCESS',
                        'data': {
                            "progress": game["players_voted"],
                            "title": curTitle
                        }
                    }
                else:
                    message = {
                        "type": "CHOOSE_ARTICLE",
                        'status': 'SUCCESS',
                        'data': {
                            "progress": game["articles_chosen"]
                        }
                    }
                self.write_message(json.dumps(message))
                message['status'] = 'BROADCAST'
                self.broadcast(game['pin'], json.dumps(message), False)
                self.save_game(game)
            elif data['type'] == "VOTE":
                if not data.get('name'):
                    self.write_message("Name field missing")
                    return
                game = self.load_game(self.pin)
                if not game.get('players_voted'):
                    game['players_voted'] = []
                if self.name in game['players_voted']:
                    self.write_message(
                        json.dumps({
                            "type": "VOTE",
                            "status": "FAILURE",
                            "data": "alreada voted"
                        }))
                    return
                if self.name == data['name']:
                    self.write_message(
                        json.dumps({
                            "type": "VOTE",
                            "status": "FAILURE",
                            "data": "can't vote for yourself"
                        }))
                    return
                game['players'][self.name]['voting'].append(data['name'])
                game['players_voted'].append(self.name)
                message = {}
                if len(game['players_voted']) == len(game['players']):
                    game['active_player'] = game['active_player'] + 1
                    if game['active_player'] == len(game['players']):
                        game['state'] = 'FINISHED'
                        evaluate_game(game)
                        message = {
                            "type": "VOTE",
                            "status": "BROADCAST",
                            "data": {
                                "progress": -1,
                                "results": filter_score(game)
                            }
                        }
                    else:
                        game['players_voted'] = []
                        curPlayer = game['order'][game['active_player']]
                        curTitle = game['players'][curPlayer]['article']
                        message = {
                            "type": "VOTE",
                            "status": "BROADCAST",
                            "data": {
                                "progress": game["players_voted"],
                                "title": curTitle
                            }
                        }
                else:
                    message = {
                        "type": "VOTE",
                        "status": "BROADCAST",
                        "data": {
                            "progress": game["players_voted"]
                        }
                    }
                self.broadcast(game['pin'], json.dumps(message))
                self.save_game(game)
            elif data['type'] == "REJOIN_GAME":
                if ('pin' not in data):
                    self.write_message("Game pin field missing")
                    return
                if ('name' not in data):
                    self.write_message("Name field missing")
                    return
                self.connections[data['pin']].append(self)
                self.name = data['name']
                self.pin = data['pin']
            else:
                self.write_message("Unrecognized type")
        except json.JSONDecodeError:
            self.write_message("Bad format")

    def on_close(self):

        print("closed")
